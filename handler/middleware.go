package handler

import (
	"context"
	"github.com/golang/protobuf/proto"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"net/http"
	"strconv"
)
// Http header keys.
const (
	HttpHeaderKeyGrpcHttpCode = "Grpc-Metadata-X-Http-Code"
)

func HttpResponseModifier(ctx context.Context,w http.ResponseWriter, _ proto.Message) error{
	md,ok := runtime.ServerMetadataFromContext(ctx)
	if !ok {
		return nil
	}

	if vals := md.HeaderMD.Get(GrpcHeaderKeyHttpCode); len(vals) > 0 {
		code, err := strconv.Atoi(vals[0])
		if err != nil {
			return err
		}
		w.WriteHeader(code)
		// delete the headers to not expose any grpc-metadata in http response
		delete(md.HeaderMD, GrpcHeaderKeyHttpCode)
		delete(w.Header(), HttpHeaderKeyGrpcHttpCode)
	}
	return nil
}