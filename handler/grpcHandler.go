package handler

import (
	"context"
	"errors"
	api_v1 "github.com/AwesomeRei/proto/go/api/v1"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/rs/zerolog/log"
	"gitlab.com/AwesomeRei/kraft-producer/service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	GrpcHeaderKeyHttpCode = "x-http-code"
)


type handler struct {
	api_v1.UnimplementedProducerServiceServer
	s service.StreamService
}


func New(service service.StreamService) (api_v1.ProducerServiceServer,error)  {
	if service == nil{
		return nil,errors.New("service must be provided")
	}
	return &handler{s: service},nil
}

func (h *handler) Health (ctx context.Context,empty *empty.Empty ) (*empty.Empty,error)  {
	log.Debug().Str("Health Handler","ok").Send()
	return empty,nil
}

func (h *handler) CreateEvent(ctx context.Context, request *api_v1.EventRequest) (*api_v1.EventResponse, error) {
	err := h.s.SendMessage(ctx,request.Topic,request.Type,request.Message)
	if err != nil{
		log.Error().Str("error creating event",err.Error()).Send()
		return nil, status.Error(codes.Internal,err.Error())
	}
	return &api_v1.EventResponse{Status: "Ok"},nil
}

