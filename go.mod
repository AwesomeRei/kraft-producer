module gitlab.com/AwesomeRei/kraft-producer

go 1.15

require (
	github.com/AwesomeRei/proto v1.0.0
	github.com/AwesomeRei/providers v0.0.0-20210508140004-e895c3bf2867
	github.com/caarlos0/env/v6 v6.5.0
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/hashicorp/levant v0.3.0 // indirect
	github.com/rs/zerolog v1.21.0
	github.com/segmentio/kafka-go v0.4.15
	github.com/stretchr/testify v1.7.0
	google.golang.org/grpc v1.37.0
)
