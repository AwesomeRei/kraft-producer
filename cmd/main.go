package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	api_v1 "github.com/AwesomeRei/proto/go/api/v1"
	"github.com/AwesomeRei/providers/pubsub/kafkaStreamer"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/segmentio/kafka-go"
	handler "gitlab.com/AwesomeRei/kraft-producer/handler"
	"gitlab.com/AwesomeRei/kraft-producer/service"
	"google.golang.org/grpc"
)

func init() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

}

var cfg Config

func main() {
	ctx := context.Background()
	config, err := LoadConfig(ctx)
	if err != nil {
		log.Err(err).Send()
	}
	cfg = config
	if cfg.IsDebuggingEnabled {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}

	if err := RunServer(ctx, fmt.Sprintf(":%d", cfg.ApiPort),
		runtime.WithForwardResponseOption(handler.HttpResponseModifier)); err != nil {
		log.Error().Str("error", err.Error()).Send()
	}
}

func newGRPCService(streamService service.StreamService) error {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", cfg.GrpcPort))
	if err != nil {
		log.Error().Str("GRPC Server Failed", err.Error()).Send()
		return err
	}
	grpcServer := grpc.NewServer()
	handler, err := handler.New(streamService)
	if err != nil {
		log.Error().Str("GRPC Handler failed", err.Error()).Send()
		return err
	}
	api_v1.RegisterProducerServiceServer(grpcServer, handler)
	log.Info().Str("GRPC Server Start", fmt.Sprintf(":%d", cfg.GrpcPort)).Send()
	return grpcServer.Serve(lis)
}

func newRESTService(ctx context.Context, address string, opts ...runtime.ServeMuxOption) error {
	server := http.NewServeMux()
	gw, err := newGateway(ctx, opts...)
	if err != nil {
		log.Error().Str("Failed to initialize Rest Service", err.Error()).Send()
		return err
	}
	server.Handle("/", gw)
	log.Info().Str("Starting Rest server", address).Send()
	return http.ListenAndServe(address, server)
}

func newGateway(ctx context.Context, opts ...runtime.ServeMuxOption) (http.Handler, error) {
	gw := runtime.NewServeMux(opts...)
	dialOpts := []grpc.DialOption{grpc.WithInsecure()}

	address := fmt.Sprintf("%s:%d", cfg.Host, cfg.GrpcPort)
	err := api_v1.RegisterProducerServiceHandlerFromEndpoint(ctx, gw, address, dialOpts)
	if err != nil {
		log.Error().Str("Error", err.Error())
		return nil, err
	}
	return gw, nil
}

func RunServer(ctx context.Context, address string, opts ...runtime.ServeMuxOption) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGTERM)
	signal.Notify(shutdown, syscall.SIGINT)

	errorChannel := make(chan error, 2)

	eventProvider, err := kafkaStreamer.New("localhost", 9092, kafkaStreamer.Options{Balancer: &kafka.LeastBytes{}})
	if err != nil {
		log.Error().Str("error creating event streamer client", err.Error()).Send()
		return err
	}

	stream, err := service.New(eventProvider)
	if err != nil {
		log.Error().Str("error creating event streamer client", err.Error()).Send()
		return err
	}

	go func() { errorChannel <- newGRPCService(stream) }()
	go func() { errorChannel <- newRESTService(ctx, address, opts...) }()

	select {
	case err := <-errorChannel:
		return err
	case <-shutdown:
		log.Info().Str("Signal Shutdown Received", "ok")
		cancel()
	case <-ctx.Done():
		log.Info().Str("Context Cancelled", "ok")
	}
	return nil
}
