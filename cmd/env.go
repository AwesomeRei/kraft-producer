package main

import (
	"context"
	"github.com/caarlos0/env/v6"
)



type Config struct {
	IsDebuggingEnabled bool   `env:"ENABLE_DEBUGGING,required"`
	Host string `env:"IP_ADDRESS,required" envDefault:"localhost"`
	ApiPort            int `env:"API_PORT,required" envDefault:"8080"`
	GrpcPort	int `env:"GRPC_PORT,required" envDefault:"8081"`
	Kafka struct{
		Topic string `env:"KAFKA_TOPIC,required"`
	}
}

func LoadConfig(ctx context.Context) (Config,error) {
	cfg:=Config{}
	err := env.Parse(&cfg)
	if err != nil{
		return Config{},err
	}
	//log.Info().Str("Config",cfg).Send()
	return cfg,nil
}