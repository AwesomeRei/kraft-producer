# kraft-producer

## How to Build
```cmd
go build -o main ./cmd/** 
```

## How to Run
```cmd
ENABLE_DEBUGGING=true KAFKA_TOPIC=jaeger ./main
```

## Environment 
full environment can be seen in env.go

ENABLE DEBUGGING 

KAFKA_TOPIC 


## MOCKING

Mocks are stored in `internal/mocks`. The [Mockery tool](https://github.com/vektra/mockery) can be used to generate mocks for testing purpose.

To install the tool:

```shell script
docker pull vektra/mockery
```

An example command that generates mocks for a vendor module:

```shell script
docker run -v $PWD:/src -w /src vektra/mockery --dir=vendor/gitlab.com/muna-lively/providers/pubsub --all --output=internal/mocks/mockpubsub --outpkg=mockpubsub
```