

// # Variables used below and their defaults if not set externally
//variables {
//  CI_REGISTRY = "registry.gitlab.com"                       # registry hostname
//  CI_REGISTRY_IMAGE = "registry.gitlab.com/awesomerei/kraft-producer"  # registry image location
//  PROJECT = "master"                             # branch name, slugged
//  TAG = "aab6cbc6963ab7341cdda1ba0e457337c810734f"                                  # repo's commit for current pipline
//  CI_PROJECT_PATH_SLUG = "kraft-producer"              # repo and group it is part of, slugged
//  CI_REGISTRY_USER = "AwesomeRei"                                     # set for each pipeline and ..
//  CI_REGISTRY_PASSWORD = "Andreandcomputer"                                 # .. allows pull from private registry
//}

job "[[ .deployment.job_name ]]" {
  datacenters = [[ .deployment.datacenters | toJson ]]
  type = "service"
  group "[[ .deployment.group_name ]]" {
    count = [[ .deployment.node_count ]]
    
    network {
      port "http" {
        to = 8080
      }
      port "grpc" {
        to = 8181
      }
    }
    service {
      name = "[[ .deployment.service_name ]]"
      port     = "http"
      check {
        name     = "HTTP Check"
        type     = "http"
        path     = "/v1/health"
        interval = "5s"
        timeout  = "2s"
      }
    }
    task "[[ .deployment.task_name ]]" {
      driver = "docker"
      
      config {
        image = "[[ .deployment.registry.image ]]:[[ or .deployment.registry.tag .tag]]"
        ports = ["http","grpc"]  
        
        auth {
          server_address = "[[ .deployment.registry.address ]]"
          username = "[[ or .deployment.registry.auth.username .username ]]"
          password = "[[ or .deployment.registry.auth.password .password ]]"
        }    
      }
      env {
        ENABLE_DEBUGGING = "[[ .deployment.env.ENABLE_DEBUGGING ]]"
        KAFKA_TOPIC = "[[ .deployment.env.KAFKA_TOPIC ]]"
      }
      
      resources {
        cpu = [[ .deployment.resource.cpu ]]
        memory = [[ .deployment.resource.memory ]]
      }
      
 
    }
  }
}
