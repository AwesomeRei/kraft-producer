package service

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/AwesomeRei/providers/pubsub"
	"gitlab.com/AwesomeRei/kraft-producer/internal/mocks/mockpubsub"
	"testing"
)

func TestNewEventStreamer(t *testing.T) {
	tests := []struct{
		name string
		provider pubsub.EventStreamer
		expected error
	}{
		{
			name: "success",
			provider: &mockpubsub.EventStreamer{},
			expected: nil,
		},
		{
			name: "error",
			provider: nil,
			expected: errors.New("event streamer must be supplied"),
		},
	}
	for _,tc := range tests{
		t.Run(tc.name, func(t *testing.T) {
			_,err := New(tc.provider)
			assert.Equal(t, tc.expected,err)
		})
	}
}

func TestEventProducer_SendMessage(t *testing.T) {
	worksProvider := &mockpubsub.EventStreamer{}
	worksProvider.On("SendEvent",mock.Anything,mock.Anything,mock.Anything).Once().Return(nil)
	errorProvider := &mockpubsub.EventStreamer{}
	errorProvider.On("SendEvent",mock.Anything,mock.Anything,mock.Anything).Once().Return(errors.New("some error"))
	tests := []struct{
		name string
		topic string
		message string
		msgType string
		service StreamService
		expected error
	}{
		{
			name: "success",
			topic: "topic",
			message: "{\"name\":\"name\", \"message\":\"message\"}",
			msgType: "ExampleMessage",
			service: &EventProducer{W: worksProvider},

			expected: nil,
		},
		{
			name: "error",
			topic: "topic",
			message: "{\"name\":\"name\", \"message\":\"message\"}",
			msgType: "ExampleMessage",
			service: &EventProducer{W: errorProvider},

			expected: errors.New("some error"),
		},
	}
	for _,tc := range tests{
		t.Run(tc.name, func(t *testing.T) {
			err := tc.service.SendMessage(context.Background(),tc.topic,tc.msgType,tc.message)
			assert.Equal(t, tc.expected,err)
		})
	}
}

