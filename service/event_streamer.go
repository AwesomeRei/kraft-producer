package service

import (
	"context"
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/AwesomeRei/providers/pubsub"
	"github.com/AwesomeRei/providers/pubsub/messages"

)

type StreamService interface {
	SendMessage(ctx context.Context,topic string,msgType string,msg string) error
}

type EventProducer struct {
	W pubsub.EventStreamer
}

func New(e pubsub.EventStreamer) (*EventProducer,error){
	if e == nil{
		return nil,errors.New("event streamer must be supplied")
	}
	return &EventProducer{W: e},nil
}

func (e *EventProducer) SendMessage(ctx context.Context,topic string,msgType string,msg string) error {
	t := messages.ToType(msgType)
	log.Debug().Str("message type",fmt.Sprint(t)).Send()
	msgData,err := t.GetStruct([]byte(msg))
	if err != nil {
		return err
	}
	msgBox,err := pubsub.NewMessageBox("id",msgData,t)
	if err != nil {
		return err
	}
	err = e.W.SendEvent(ctx,topic,msgBox)
	if err != nil {
		log.Error().Str("topic",topic).Str("error",err.Error()).Msgf("%+v",msgBox)
		return err
	}
	return nil
}